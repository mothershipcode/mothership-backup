Mothership Backup
=================

Mothership backup is a simple shell script that backs up mongo databases.

#Usage
`sh backup.sh remote_user remote_address remote_database remote_dump local_dump`
See `example.sh` for real usage example.

#Cron Enrty
`@hourly /bin/sh /opt/mothership-backup/example.sh>>/tmp/cron_debug_log.log`
If you're using cron with root (which you probably are), ALL paths MUST be absolute. (See nexleader-ipsat-backup.sh for example)