#!/usr/bin/env bash

user=$1
remote=$2
db=$3
remoteDump=$4
localDump=$5
now=$(date)

/bin/mkdir $localDump

/usr/bin/printf "Backup of $user@$remote started at $now\n"

/usr/bin/printf "Clearing local dump..."
/bin/rm -r "$localDump/dump"
/bin/mkdir "$localDump/dump"
/usr/bin/printf "Done.\n"

/usr/bin/printf "Backup of $user@$remote started at $now\n">>"$localDump/log.log"

/usr/bin/printf "Dumping DB on remote server...\n"
/usr/bin/ssh $user@$remote "mongodump --db $db --out $remoteDump"
/usr/bin/printf "Done.\n"

/usr/bin/printf "Copying remote files to local dump..."
/usr/bin/scp -r $user@$remote:$remoteDump "$localDump"
/usr/bin/printf "Done.\n"

/usr/bin/printf "Copying local dump to timestamp..."
/bin/cp -r "$localDump/dump" "$localDump/$now"
/usr/bin/printf "Done.\n"

/usr/bin/printf "Backup of $user@$remote ended at $now\n"
/usr/bin/printf "Backup of $user@$remote ended at $now\n">>"$localDump/log.log"